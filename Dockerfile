ARG GOLANG_VERSION=1.18
ARG ALPINE_VERSION=3.15

FROM golang:${GOLANG_VERSION}-alpine AS builder

# Download athens to build from source
ENV ATHENS_VERSION=0.11.0
RUN apk add --update make git
RUN git clone --branch v$ATHENS_VERSION https://github.com/gomods/athens.git $GOPATH/src/github.com/gomods/athens
WORKDIR $GOPATH/src/github.com/gomods/athens

# Build
RUN DATE="$(date -u +%Y-%m-%d-%H:%M:%S-%Z)" && GO111MODULE=on CGO_ENABLED=0 GOPROXY="https://proxy.golang.org" go build -ldflags "-X github.com/gomods/athens/pkg/build.version=$ATHENS_VERSION -X github.com/gomods/athens/pkg/build.buildDate=$DATE" -o /bin/athens-proxy ./cmd/proxy

FROM alpine:${ALPINE_VERSION}
ENV GO111MODULE=on

COPY --from=builder /bin/athens-proxy /bin/athens-proxy
COPY --from=builder /go/src/github.com/gomods/athens/config.dev.toml /config/config.toml
COPY --from=builder /usr/local/go/bin/go /bin/go

RUN chmod 644 /config/config.toml

# Add tini, see https://github.com/gomods/athens/issues/1155 for details.
RUN apk add --update git git-lfs mercurial openssh-client subversion procps fossil tini && \
	mkdir -p /usr/local/go

EXPOSE 3000
ENTRYPOINT [ "/sbin/tini", "--" ]
CMD ["athens-proxy", "-config_file=/config/config.toml"]
