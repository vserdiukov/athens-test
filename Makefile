GO               = go
TARGET_DIR       ?= $(PWD)/.build
GOBIN			 ?= $(PWD)/bin

.PHONY: test
test:
	$(GO) mod download

.PHONY: build
build: $(GOBIN)
	@GOBIN=$(GOBIN) $(GO) install \
		github.com/gomods/athens/cmd/proxy@master

.PHONY: $(GOBIN)
$(GOBIN):
	@mkdir -p $(GOBIN)